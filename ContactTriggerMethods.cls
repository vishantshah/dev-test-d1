public class ContactTriggerMethods {

    public static boolean executingAfterInsert = false;
    
    /*
    * After a Contact is Inserted
    * Creates a Case and a Task based on the Level of Contact.
    *  
    * Returns nothing
    */
    public static void afterInsert(list<Contact> contacts){
        
        list<case> cases = new list<case>();
        list<task> tasks = new list<task>();
        
        string caseStatus = 'Working';
        string caseOrigin = 'New Contact';
        
        for(Contact c : contacts){
        
            id ownerId = c.OwnerId;
            string priority;

            if (c.Level__c == 'Primary')
                priority = 'High';
            else if (c.Level__c == 'Secondary')
                priority = 'Medium';
            else 
                priority = 'Low';
            
            cases.add(Utilities.createCase(caseOrigin, c.Id, c.Account.Id, caseStatus, ownerId, priority));
        }
        
        insert cases;
        
        //Querying for Cases as the Case number needed for subject is only available after Cases are inserted.
        //Extracting Account and Contact Name to save on more queries.
        for (Case cse : [select OwnerId, AccountId, ContactId, Contact.Name, CaseNumber, priority 
                           from Case 
                          where id in :cases]){
            
            date activityDate;
            
            if (cse.priority == 'High')
                activityDate = date.today() + 7;
            else if (cse.priority == 'Medium')
                activityDate = date.today() + 14;
            else 
                activityDate = date.today() + 21;
                              
            string taskSubject =  string.format('Welcome call for {0} - {1}.', new string[]{cse.Contact.Name, cse.CaseNumber});
            tasks.add(Utilities.createTask(activityDate, cse.AccountId, cse.ContactId, taskSubject, taskSubject, cse.OwnerId, 'Other'));
        }
        
        insert tasks;
    } 
    
}