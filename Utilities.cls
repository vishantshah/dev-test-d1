public class Utilities {

    /*
    * Creates a new Task
    * Created it as a utility method to make it available for other classes.
    * Returns Task
    */
    public static task createTask(date activityDate, id whatId, id whoId, string subject, string description, string ownerId, string taskType){
       
        Task newTask = new Task();
        
        newTask.ActivityDate = activityDate;
        newTask.WhatId = whatId;
        newTask.WhoId = whoId;
        newTask.Type = taskType;
        newTask.Subject = subject;
        newTask.Description = description;
        newTask.OwnerId = ownerId;
        
        return newTask;
    } 

    /*
    * Creates a new Case
    * Created it as a utility method to make it available for other classes.
    * Returns Case
    */
    public static Case createCase(string origin, id contactId, id accountId, string status, id ownerId, string priority){
        
        Case newCase = new Case();
        
        newCase.Origin = origin;
        newCase.ContactId = contactId;
        newCase.AccountId = accountId;
        newCase.Status = status;
        newCase.OwnerId = ownerId;
        newCase.Priority = priority;
        
        return newCase;
    }
    
}