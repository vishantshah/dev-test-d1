@IsTest
public class ContactTriggerMethodsTest {

    public Static testMethod void testContactTrigger(){
        
        list<Account> accounts = new list<Account>();
        list<Contact> contacts = new list<Contact>();
        
        for (integer i = 0; i < 200; i ++){
            Account a = new Account();
            a.Name = 'Test ' + string.valueOf(i);
            accounts.add(a);
        }
        
        insert accounts;
        
        integer i = 0;
        string level = 'Primary';
        
        test.startTest();
        
        for (Account a : accounts){
            Contact con = new Contact();
            con.FirstName = 'FirstName ' + i;
            con.LastName = 'LastName ' + i;
            con.AccountId = a.id;
            
            system.debug(level);
            if (level == 'Primary'){
                con.Level__c = 'Primary';
                level = 'Secondary';
            } else if (level == 'Secondary'){
                con.Level__c = 'Secondary';
                level = 'Tertiary';
            } else if (level == 'Tertiary'){
                con.Level__c = 'Tertiary';
                level = 'Primary';
            }
            
            i++;
            contacts.add(con);
        }
        insert contacts;
        
        test.stopTest();
        
        list<Contact> insertedContacts = [select id, Level__c, (select id, Priority, Status from Cases), (select id, ActivityDate from Tasks) from Contact where id in :contacts ];
        
        integer primaryContacts = 0;
        integer secondaryContacts = 0;
        integer tertiaryContacts = 0;
        integer highPriority = 0;
        integer mediumPriority = 0;
        integer lowPriority = 0;
        integer casesCreated = 0;
        integer tasksCreated = 0;
        
        for (contact c : insertedContacts){
            
            if (c.Level__C == 'Primary')
            	primaryContacts ++;
            if (c.Level__C == 'Secondary')
            	secondaryContacts ++;
            if (c.Level__C == 'Tertiary')
            	tertiaryContacts ++;
            
            casesCreated += c.Cases.Size();
            for (Case cse : c.Cases){
                if (cse.Priority == 'High')
                    highPriority ++;
                if (cse.Priority == 'Medium')
                    mediumPriority ++;
                if (cse.Priority == 'Low')
                    LowPriority ++;
            }
            
            tasksCreated += c.Tasks.Size();
        }
        
        system.assertNotEquals(0, primaryContacts,'Could not create Primary Contacts');
        system.assertNotEquals(0, secondaryContacts,'Could not create Secondary Contacts');
        system.assertNotEquals(0, tertiaryContacts,'Could not create Tertiary Contacts');
        
        system.assertEquals(primaryContacts, highPriority, 'High Priority not set correctly on contact creation');
        system.assertEquals(secondaryContacts, mediumPriority, 'Medium Priority not set correctly on contact creation');
        system.assertEquals(tertiaryContacts, lowPriority, 'Low Priority not set correctly on contact creation');
            
        system.assertEquals(200, tasksCreated,'Could not create Tasks on Contact insertion');   
        system.assertEquals(200, casesCreated,'Could not create Cases on Contact insertion');   
        
    }
    
}